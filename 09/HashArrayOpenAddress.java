class HashArrayOpenAddress {
    static final int SzBkt = 11;
    static final int Shift = 7;
    
    static int String2Integer(String s) {
	int result = 0;

	result = (int)s.charAt(0) - (int)'a';
    
	return result;
    }
    
    static int HashFunction(int l) {
	return l % SzBkt;
    }
    
    public static void main (String[] args) {
	String [] persons = {"abe",        // 0 
			     "amagasa",    // 0
			     "sannomiya",  // 18
			     "sakuma",     // 18
			     "maeda",      // 12
			     "sato",       // 18
			     "tadano",     // 19
			     "sano",       // 18
			     "sugiki",     // 18
			     "oka"};       // 14

	String [] HashTable = new String[SzBkt];

	// Constructing a hash structure
	for (int i = 0; i < persons.length; i++) {
	    int s2int = String2Integer(persons[i]);
	    int pos = HashFunction(s2int);

	    // Write your code here (collision avoidance)
	    while(HashTable[pos] != null){
		pos = (pos+Shift) % SzBkt; // 1個先にする
	    }
	    HashTable[pos] = new String(persons[i]); // Data insertion
	}
    
	for (int i = 0; i < SzBkt; i++) {
	    System.out.println("Position " + i + "\t" + HashTable[i]);
	}
    }
}
