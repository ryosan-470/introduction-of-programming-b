import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
class StringSort {
    public static void main(String args[]){
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	String[] sort = new String[1000]; 
	int linecounter = 0;	
	// String sort[] = {"kawasima","maeda","nishimura","taro","hanako","john"};
	// int sortnum = sort.length;
	// stringSort(sort, sortnum);
	// for(int i=0; i<sortnum; i++)
	//     System.out.println(sort[i]);
	try {
	    String line;

	    while((line = reader.readLine()) != null){
		sort[linecounter] = line;
		linecounter++;
	    }
		stringSort(sort,linecounter);

	    for(int i=0; i<linecounter; i++){
		System.out.println(sort[i]);
	    }
	}catch(IOException e){
	    System.out.println(e);
	}


    }

    public static void stringSort(String[] a, int length){
	for(int i=0; i<length-1; i++){
	    for(int j=i+1; j<length; j++){
		// 前が辞書的に後ろである
		if(a[i].compareTo(a[j]) > 0){
		    String tmp = a[i];
		    a[i] = a[j];
		    a[j] = tmp;
		}
	    }
	}
    }
}
