public class Rectangle2 {
    int width;
    int height;

    Rectangle2() {
	setSize(10, 20);
    }

    @Override
    public String toString() {
	return "[ width = " + width + ", height = " + height + " ]";
    }

    void setSize(int w, int h) {
	if(w < 0)
	    w = 0;	
	if(h < 0)
	    h = 0;	
	width = w;
	height = h;
    }
    int getArea() {
	return width * height;
    }

    public static void main(String[] args) {
	Rectangle2 r = new Rectangle2();
	r.setSize(1, 2);
	System.out.println(r);
    }
}
