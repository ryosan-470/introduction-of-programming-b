import java.io.InputStreamReader;
import java.io.BufferedReader;
/*
 * プログラム入門B 設問3
 *
 */
class Fib {
    public static void main(String args[]){
	//Initialized
	int fn;

	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	try{
	    String line = reader.readLine();
	    int i = Integer.parseInt(line);
	    fn = fn(i);
	    System.out.println(i+"番目のフィボナッチ数は"+fn+"です");
	}catch(Exception e){
	    System.out.println(e);
	}
    }
    public static int fn(int x){
	if(x == 0)
	    return 0;
	else if(x == 1)
	    return 1;
	else {
	    return fn(x-1)+fn(x-2);
	}
    }
}