import java.util.Scanner;
/*
 * プログラム入門B 設問4
 * 10進数を2進数にする
 */
class BinaryNotRecursive  {
    public static void main(String args[]){
	//Initialized
	int i = 0;
	int ans[] = new int[100];
	Scanner sc = new Scanner(System.in);
	int x = sc.nextInt();
	while(x > 0){
	    ans[i] = x % 2;
	    x /= 2;
	    i++;
	}
	for(int j=i-1; j >= 0; j--)
	    System.out.print(ans[j]);
	System.out.println();
	
    }
}
