// encoding UTF-8
class ListString
{
    String name;
    ListString next;

    // Constructor
    ListString(String name, ListString tail) {
	this.name = new String(name); 
	this.next = tail;
    }

    static ListString Insert(String s, ListString list) {
	return new ListString(s, list);
    }

    static void Display(ListString list) {
	while(list!=null) {
	    System.out.print(list.name+"-->");
	    list=list.next;
	}
	System.out.println("null");
    }

    // Q1
    static int length(ListString list) {
	int l = 0;
	while(list != null){
	    l++;
	    list = list.next;
	}
	return l;
    }

    // Q2
    static void DisplayReverse(ListString list)
    {
	String tmp[] = new String[length(list)];
	int n = tmp.length;
	for (int i = 0; i < n; i++) {
	    tmp[i] = list.name;	    
	    list = list.next;
	}
	System.out.print("null");
	for (int i = n-1; i >= 0; i--) 
	    System.out.print("<--"+tmp[i]);	
	System.out.println();
    }

    // Q3
    static ListString Delete(String s, ListString list)
    {
	// If list is empty
	if(list == null)
	    return list;

	// If list element is head
	if(list.name.equals(s))
	    list = list.next;
	
 	ListString ptr = list;      // Tracking Node
	ListString pre = list.next; // Pre Tracking Node
	while(pre != null && !((pre.name).equals(s))){
	    ptr = pre;
	    pre = ptr.next;
	}
	if(pre != null)
	    ptr.next = pre.next;
	return list;
    }    
}
class ListJava{
    public static void main (String[] args)
    {
	ListString l;

	l = new ListString("Tokyo",null);
	l = new ListString("Kyoto",l);
	l = new ListString("Tsukuba",l);
	l = new ListString("Nara",l);
	l = new ListString("Ueno",l);
	l = new ListString("Ehime",l);

	ListString.Display(l);
	System.out.println("Number of Elements: " + ListString.length(l));
	ListString.DisplayReverse(l);
	l = ListString.Delete("Tokyo", l);
	ListString.Display(l);
    }
}
