import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
class HashArrayOpenAddress {
    static final int SzBkt = 11;
    static final int Shift = 7;
    
    public  static int String2Integer(String s) {
	int result = 0;

	result = (int)s.charAt(0) - (int)'a';
    
	return result;
    }
    
    static int HashFunction(int l) {
	return l % SzBkt;
    }
    static boolean Search(String w){
	int h = HashFunction(String2Integer(w));
	return false;
    }

    public static void main (String[] args) {
	String [] persons = {"abe",        // 0 
			     "amagasa",    // 0
			     "sannomiya",  // 18
			     "sakuma",     // 18
			     "maeda",      // 12
			     "sato",       // 18
			     "tadano",     // 19
			     "sano",       // 18
			     "sugiki",     // 18
			     "oka"};       // 14

	String [] HashTable = new String[SzBkt];
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	try {
	    // Constructing a hash structure
	    for (int i = 0; i < persons.length; i++) {
		int s2int = String2Integer(persons[i]);
		int pos = HashFunction(s2int);

		// Write your code here (collision avoidance)
		while(HashTable[pos] != null){
		    pos = (pos+Shift) % SzBkt; // 1個先にする
		}
		HashTable[pos] = new String(persons[i]); // Data insertion
	    }
    
	    for (int i = 0; i < SzBkt; i++) {
		System.out.println("Position " + i + "\t" + HashTable[i]);
	    }
	    
	    String line;
	    do{
		System.out.print("> ");
		line = br.readLine();
		if(line.equals("search")){
		    System.out.print("[search] data ? ");
		    String key = br.readLine();
		    search(HashTable, key);
		    dump(HashTable);
		}
		if(line.equals("insert")){
		    System.out.print("[insert] data ? ");
		    String key = br.readLine();
		    insert(HashTable, key);		
		    dump(HashTable);			
		}
		if(line.equals("delete")){
		    System.out.print("[delete] data ? ");
		    String key = br.readLine();
		    delete(HashTable, key);		
		    dump(HashTable);
		}
	    }while(!line.equals("exit"));
	    
	} catch (IOException e) {
	}
	
    }
    public static void dump(String[] table){
	for (int i = 0; i < table.length; i++) {
	    System.out.println("Position " + i + "\t" + table[i]);
	}	
    }
    // ハッシュテーブル内のkeyをサーチする
    public static void search(String[] table, String key){
	boolean found = false;
	int keyhash = HashFunction(String2Integer(key));

	int count = SzBkt;
	while(!found == true && count > 0){
	    if(key.equals(table[keyhash])){
		found = true;
		break;
	    }else{
		keyhash = (keyhash + Shift) % SzBkt;
		count--;
	    }
	}
	if(!found)
	    System.out.println("Not found");
	else
	    System.out.println("Found :"+key);
	
    }
    // ハッシュテーブル内の空に挿入する
    public static void insert(String[] table, String key){
	boolean found = false;
	int keyhash = HashFunction(String2Integer(key));
	int count = SzBkt;
	while(found == false && count > 0){
	    if(table[keyhash] == null)
		found = true;
	    else {
		keyhash = (keyhash + Shift) % SzBkt;
		count--;
	    }
	}
	if(found)
	    table[keyhash] = new String(key);
	else
	    System.out.println("Insertion Error!!");
    }
    // ハッシュテーブル内のデータを削除する
    public static void delete(String[] table, String key){
	boolean found = false;
	int keyhash = HashFunction(String2Integer(key));
	int count = SzBkt;
	while(found == false && count > 0){
	    if(table[keyhash].equals(key))
		found = true;
	    else {
		keyhash = (keyhash + Shift) % SzBkt;
		count--;
	    }
	}
	if(found)
	    table[keyhash] = null;
	else
	    System.out.println("Error! KW is not found");
    }
}
