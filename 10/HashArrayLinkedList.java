import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
class HashArrayLinkedList {
    static final int SzBkt = 5;

    static int String2Integer(String s) {
	int result = 0;
    
	result = (int)s.charAt(0);

	return result;
    }

    static int HashFunction(int l) {
	return l % SzBkt;
    }

    public static void main (String[] args) {
	String [] persons = {"abe",        // 0 
			     "amagasa",    // 0
			     "sannomiya",  // 18
			     "sakuma",     // 18
			     "maeda",      // 12
			     "sato",       // 18
			     "tadano",     // 19
			     "sano",       // 18
			     "sugiki",     // 18
			     "oka"};       // 14

	ListString [] HashTable = new ListString[SzBkt];
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

	try{
	    for (int i = 0; i < SzBkt; i++) {
		HashTable[i] = null;
	    }

	    // Constructing a hash structure
	    for (int i = 0; i < persons.length; i++) {
		int s2int = String2Integer(persons[i]);
		int pos = HashFunction(s2int);
		HashTable[pos] = ListString.Insert(persons[i], HashTable[pos]);
	    }
    
	    dump(HashTable, SzBkt);

	    String line;
	    do{
		System.out.print("> ");
		line = br.readLine();
		if(line.equals("search")){
		    boolean found = false;
		    System.out.print("[search] data ? ");
		    String key = br.readLine();
		    for (int i = 0; i < SzBkt; i++) {
			if(ListString.Search(HashTable[i], key)) {
			    found = true;
			    break;
			}else
			    found = false;
		    }
		    if(found)
			System.out.println("Found :"+key);
		    else
			System.out.println("Not Found");
		    dump(HashTable, SzBkt);
		}
		
		if(line.equals("insert")){
		    System.out.print("[insert] data ? ");
		    String key = br.readLine();
		    // ハッシュを求める
		    int keyhash = HashFunction(String2Integer(key));		
		    HashTable[keyhash] = ListString.Insert(key, HashTable[keyhash]);
		    dump(HashTable, SzBkt);
		}
		if(line.equals("delete")){
		    System.out.print("[delete] data ? ");
		    String key = br.readLine();
		    int keyhash = HashFunction(String2Integer(key));
		    HashTable[keyhash] = ListString.Delete(key, HashTable[keyhash]);
		    dump(HashTable, SzBkt);
		}
	    }while(!line.equals("exit"));
	}catch(IOException e){

	}
    }
    public static void dump(ListString[] table, int size){
	for (int i = 0; i < size; i++) {
	    ListString.Display(table[i]);
	}
}
}
class ListString
{
    String name;
    ListString next;

    ListString(String name, ListString tail) {
	this.name=new String(name); 
	this.next=tail;
    }

    static ListString Insert(String s, ListString list) {
	return new ListString(s, list);
    }

    static void Display(ListString list) {
	while(list!=null) {
	    System.out.print(list.name+"-->");
	    list=list.next;
	}
	System.out.println("null");
    }
    static boolean Search(ListString list, String key){
	while(list != null){
	    if(key.equals(list.name))
		return true;
	    list = list.next;
	}
	return false;
    }

    static ListString Delete(String s, ListString list)
    {
	ListString head = list;
	ListString next = list.next;
	if (head.name.equals(s)) 
	    return head.next;	

	while (next != null) {
	    if (next.name.equals(s)) {
		list.next = list.next.next;
		break;
	    }
	    list = list.next;    
	    next = next.next;    
	}
	return head;
    }
}
