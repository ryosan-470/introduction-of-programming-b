import java.io.*;

class CharlnBoth {
    public static void main(String args[]){
	//同じ文字列があるかどうか
	boolean found = false;
	
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	try {
	    String Beforeline = reader.readLine();
	    String Afterline = reader.readLine();
	    outer:
	    for(int i=0; i<Beforeline.length(); i++){
		for(int j=0; j<Afterline.length(); j++){
		    if(Beforeline.charAt(i) == Afterline.charAt(j)){
			System.out.println("同じ文字'"+Beforeline.charAt(i)+"'が見つかりました");
			found = true;
			break outer;
			
		    }
		}
	    }
	    if(!found)
		System.out.println("同じ文字が見つかりませんでした");

	}catch(IOException e){
	    System.out.println(e);
	}catch(Exception e){
	    System.out.println(e);
	}
    }
}
