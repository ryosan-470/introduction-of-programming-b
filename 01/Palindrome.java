import java.io.*;

class Palindrome {
    public static void main(String args[]){
	//回分かどうかを判定する
	boolean hantei = true;
	System.out.println("文字列を入力してください");
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	try {
	    String line = reader.readLine();
	    for(int i=0; i < line.length()/2; i++){
		if(line.charAt(i) != line.charAt(line.length() -1 -i)){
		    System.out.println("'"+line+"'は回文ではありません");
		    hantei = false;
		    break;
		}
	    }
	if(hantei)
	    System.out.println("'"+line+"'は回文です");
	}catch(IOException e){
	    System.out.println(e);
	}catch(Exception e){
	    System.out.println(e);
	}
    }
}
