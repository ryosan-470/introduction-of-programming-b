public class InnerProduct {
    public static void main(String[] args){
	double[] A = {1.0, 1.5, 2.0, 2.5};
	double[] B = {3.1, 2.1, 1.1, 0.1};
	double ans = 0;
	for(int i=0; i<A.length; i++)
	    ans += (A[i]*B[i]);
	System.out.printf("内積は%07fです",ans);
	System.out.println();
    }   
}
