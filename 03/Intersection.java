import java.io.InputStreamReader;
import java.io.BufferedReader;

public class Intersection {
    public static void main(String[] args){
	
	int[] myArrayA = new int[10];
	int[] myArrayB = new int[10];
	System.out.println("myArrayA:");
	inputMethod(myArrayA);
	System.out.println("myArrayB:");
	inputMethod(myArrayB);

	intersectionMethod(myArrayA, myArrayB);
    }

    public static void inputMethod(int[] input){
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	try{
	    for(int i=0; i<10; i++){	    
		String line = reader.readLine();
		input[i] = Integer.parseInt(line);
	    }
	}catch(Exception e){
	}
    }
    public static void intersectionMethod(int[] x, int[] y){
	System.out.println("Result:");
	for(int i=0; i<x.length;i++){
	    for(int j=0; j<x.length; j++){
		if(x[i]==y[j]){
		    System.out.println(x[i]);
		    break;
		}
	    }
	}
    }   
}
