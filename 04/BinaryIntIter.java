import java.io.*;

public class BinaryIntIter
{
    public static int binarySearch(int[] a, int left, int right, int x) {
	do {
	    int mid = (left + right)/2;
	    if(a[mid] == x)
		return mid;
	    else if(mid < x)
		left = mid +1;
	    else 
		right = mid;
	}while(left <= right);
	return -1;
   }
    
    public static void main(String args[]) {
        int[] myArray = new int[10];
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String line;
	System.out.print("Input 10 data: ");
        try {
	    for(int i=0; i<myArray.length; i++){
		line = reader.readLine();
		myArray[i] = Integer.parseInt(line);
	    }
            System.out.print("Input key: ");

            line = reader.readLine();
            int key = Integer.parseInt(line);

            int p; // position
            p = binarySearch(myArray, 0, myArray.length, key);
            if (p == -1) {
                System.out.println("Not Found");
            }
            else {
                System.out.println("Found at myArray[" + p + "] " + myArray[p]);
            }
        } catch (IOException e) {
        }
    }
}
